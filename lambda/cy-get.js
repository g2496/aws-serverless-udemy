const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({ region: "ap-southeast-1", apiVersion: '2012-08-10' });
const cisp = new AWS.CognitoIdentityServiceProvider({apiVersion: '2016-04-18'});

exports.handler = (event, context, callback) => {
  const accessToken = event.accessToken;
  const type = event.type;
  if(type == "all") {
 
    const params = { TableName: "cy-data" };
	dynamodb.scan(params, function(err, data) {
	  if (err) {
		console.log(err);
		callback(err);
  	  } else {
  	  	console.log(JSON.stringify(data));
		const items = data.Items.map((dataField) => {
		  return {age: parseInt(dataField.age.N), height: parseInt(dataField.height_cm.N), income: parseInt(dataField.income_day.N)};
		});

  	    callback(null, items);
  	  }
    }); 
   } else if(type === 'single') {
    //getItem
     const cispParams = {
    	"AccessToken": accessToken
     }
     cisp.getUser(cispParams, (err, result) => {
     	if(err) {
		  console.log(err);	
     	  callback(err);
     	} else {
     	  console.log(result);
     	  const userId = result.UserAttributes[0].Value;
		  const params = {
	        Key: { "UserId": { S: userId } },
		    TableName: "cy-data"
	      };
		  dynamodb.getItem(params, function(err, data) {
			if (err) {
			  console.log(err);
			  callback(err);
	  	    } else {
	          console.log(data);  	    
	  	      callback(null, {age: parseInt(data.Item.age.N), height: parseInt(data.Item.height_cm.N), income: parseInt(data.Item.income_day.N)});
	  	    }
		  });
     	}
    });
	} else {
		console.log("Something went wrong.");
	}
};
