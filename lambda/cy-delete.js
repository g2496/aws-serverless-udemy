const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({ region: "ap-southeast-1", apiVersion: '2012-08-10' });

exports.handler = (event, context, callback) => {  
	const accessToken = event.accessToken;
  const params = {
    Key: { "UserId": { S: event.userId } },
		TableName: "cy-data"
	};
	dynamodb.deleteItem(params, function(err, data) {
	  if (err) {
	    console.log(err);
	    callback(err);
    } else {
      console.log(data);  	    
      callback(null, data);
    }
	});
};
