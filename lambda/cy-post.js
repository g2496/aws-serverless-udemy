const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({ region: "ap-southeast-1", apiVersion: '2012-08-10' });

exports.handler = (event, context, callback) => {
	const params = {
	  Item: {
		  "UserId": {
				S: event.userId
			},
			"age": {
				N: event.age
			},
			"height_cm": {
				N: event.height
			},
			"income_day": {
				N: event.income
			}
		},
		TableName: "cy-data"
	};
	dynamodb.putItem(params, function(err, data) {
		if (err) {
			console.log(err);
			callback();
  	}
		else {
			console.log(data);
			callback(null, data);
	  }
	});
};
